import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class NewsService {
   
    constructor(private http: HttpClient) {}

    getNews(paramUrl){
         return this.http.get('http://beta.newsapi.org/v2/top-headlines?' +'sources=' + paramUrl +'&' +'apiKey=1bad16bc99c8441dabfcb44c49142d6b');
     }
}
