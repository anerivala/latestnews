import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-news-detail',
  templateUrl: 'news-detail.html',
})
export class NewsDetailPage {
  title;
  author;
  date;
  url;
  detail;
  image;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.title = this.navParams.get('title');
    this.author = this.navParams.get('author');
    this.date = this.navParams.get('date');
    this.url = this.navParams.get('url');
    this.detail = this.navParams.get('detail');
    this.image = this.navParams.get('image');
  }
}
