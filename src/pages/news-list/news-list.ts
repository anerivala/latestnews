import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NewsDetailPage } from '../news-detail/news-detail';
import { NewsService } from '../../news.service';

@IonicPage()
@Component({
  selector: 'page-news-list',
  templateUrl: 'news-list.html',
})

export class NewsListPage {
  paramUrl;
  newsData;
  setTitle
  constructor(public navCtrl: NavController, public newsService: NewsService, public navParams: NavParams) {
    this.paramUrl = this.navParams.get('paramUrl');
    if (this.paramUrl == "techcrunch") {
      this.setTitle = "Technical news";
    } else if (this.paramUrl == "reddit-r-all") {
      this.setTitle = "Reddit News";
    }
    else if (this.paramUrl == "bbc-news") {
      this.setTitle = "BBC News";
    }
    else if (this.paramUrl == "bbc-sport") {
      this.setTitle = "BBC Sports News";
    }
    else if (this.paramUrl == "the-times-of-india") {
      this.setTitle = "The times of India";
    }
    else if (this.paramUrl == "google-news-in") {
      this.setTitle = "Google news";
    }
    this.getNews();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewsListPage');
  }
  newsDetail(title, author, date, url, detail, image) {
    this.navCtrl.push(NewsDetailPage, {
      'title': title,
      'author': author,
      'date': date,
      'url': url,
      'detail': detail,
      'image': image
    });
  }
  getNews() {
    this.newsService.getNews(this.paramUrl).subscribe(response => {
      this.newsData = response;
      console.log(this.newsData);
    });
  }
}
