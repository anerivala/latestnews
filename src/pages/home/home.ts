import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NewsListPage } from '../news-list/news-list';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})

export class HomePage {
  newsData;
  constructor(public navCtrl: NavController) {}

  newsList() {
    this.navCtrl.push(NewsListPage);
  }

  getNews(paramUrl) {
    this.navCtrl.push(NewsListPage,{
      'paramUrl': paramUrl
    });
  }
}
